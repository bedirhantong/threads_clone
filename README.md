# Threads App Clone

This is a clone app of Threads app but with customized features.


## Table of Contents

* [Features](#features)
* [Setup](#setup)
* [Contribution](#contribution)
* [Technologies](#technologies)
* [Future additions](#future)
* [Screenshots](#screenshots)



## Features

- Share what you feel today or delete what you have shared

- Customize your profile

- You can like or comment on others posts

- You can follow anyone you want

- You can have either public or private profile



## Setup
- If you are a developer, you can clone the project from the required IDEs and run it on your computer.
  - Clone this repository: `https://github.com/bedirhantong/threads_clone.git`
- If you want to use the application as a user, wait for the new version to be released :)

## Contribution

If you would like to contribute to this project, please follow the steps below:

1. Fork this repository.
2. Clone the forked repository on your own computer: `https://github.com/bedirhantong/threads_clone.git`
3. Make and test your changes.
4. Create a pull request to upload your changes.
5. After going through the review process, your changes will be merged into the main project.

## Technologies

- MVVM
- Google Fonts
- Pub.dev packages
  - Flutter Native Splash
  - Liquid Swipe
  - Smooth Page Indicator
  - Flutter Otp Text Field


## Future

This app allows:

- [x] Login and Register
- [ ] Share your feelings
- [ ] Like or comment to others posts
- [ ] Follow your friends
- [ ] Repost others posts
- [ ] Dark and Light Theme
- [ ] Using Getx package for navigation with animation
- [ ] Responsive splash screen

## Screenshots

|                  0000                   |                  0001                   |                  0010                   |                      0011                      |
|:----------------------------------------:|:-----------------------------------------:|:----------------------------------------:|:-------------------------------------------------:|
| ![](assets/images/screenshots/first.png) | ![](assets/images/screenshots/second.png) | ![](assets/images/screenshots/third.png) | ![](assets/images/screenshots/fourth_welcome.png) |

|                      0100                      |                  0101                   |                  0111                   |                      1000                      |
|:-----------------------------------------------:|:-----------------------------------------:|:--------------------------------------:|:------------------------------------------------:|
| ![](assets/images/screenshots/login_screen.png) | ![](assets/images/screenshots/second.png) | ![](assets/images/screenshots/otp.png) | ![](assets/images/screenshots/signup_screen.png) |

|                      1001                      |                  1010                   |                  1011                   |                      1100                      |
|:-----------------------------------------------:|:-----------------------------------------:|:--------------------------------------:|:------------------------------------------------:|
| ![](assets/images/screenshots/otp.png) | ![](assets/images/screenshots/reset_via_email.png) | ![](assets/images/screenshots/reset_via_phone.png) | ![](assets/images/screenshots/enter_verification_code.png) |

